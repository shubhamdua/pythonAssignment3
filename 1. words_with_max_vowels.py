import sys

# runs only for file in same directory
# filename stores name of the file a sstring which will be passed to open()

filename = sys.argv[-1]

dic = {}
f = open(filename)
r = f.read().split()
for wrd in r:
    vowel = 0
    for i in wrd:
        if i in ["A","E","I","O","U","a","e","i","o","u"]:
            vowel += 1
    dic.update({ wrd : vowel})
max = []
mx = 0
for key in dic:
    if dic[key] > mx:
        mx = dic[key]
for key in dic:
    if dic[key] == mx:
        print mx," ",key,"\n"